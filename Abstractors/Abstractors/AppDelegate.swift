//
//  AppDelegate.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import UIKit
import TwitterKit

var sharedDelegate: AppDelegate? { return (UIApplication.shared.delegate as? AppDelegate)}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    private var blurEffectView:UIVisualEffectView?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.setupViewController()
        self.twitterSetup()
        self.window?.makeKeyAndVisible()
        return true
    }
    func setupViewController(){
        // Override point for customization after application launch.
        if self.window == nil {
            self.window = UIWindow(frame: UIScreen.main.bounds)
        }
        var vc : UIViewController? = nil
        if GeneralUtils.isUserLogin() {
            if ConfigurationKeys.TouchIdEnabled {
                vc = UIStoryboard.getLocalLoginViewController()
            } else {
                vc = UIStoryboard.getFeedsViewController()
            }
        } else {
            vc = UIStoryboard.getLoginViewController()
        }
        let navigationController = UINavigationController(rootViewController: vc!)
        self.window?.rootViewController = navigationController
    }
    func twitterSetup(){
        TWTRTwitter.sharedInstance().start(withConsumerKey: StringConstants.twitterConsumerKey, consumerSecret: StringConstants.twitterConsumerSecret)
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        application.ignoreSnapshotOnNextApplicationLaunch()
        NotificationCenter.default.post(name: Notification.Name(ConfigurationKeys.applicationWillResignActive), object: nil)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NotificationCenter.default.post(name: Notification.Name(ConfigurationKeys.applicationDidBecomeActive), object: nil)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if userActivity.webpageURL != nil{
                self.openMyURl(url: userActivity.webpageURL!)
            }
            
        }
        return true
    }
    func getQueryParameters(query: String?) -> [String: String]{
        var params = [String:String]()
        if (query == nil || query!.isEmpty) {
            return params;
        } else {
            let arr = query?.components(separatedBy: "&")
            for parameter in arr! {
                let keyValuePair = parameter.components(separatedBy: "=")
                let name = keyValuePair.count >= 1 ? keyValuePair[0] : nil
                let value = keyValuePair.count >= 2 ? keyValuePair[1] : nil
                if name != nil && name!.isEmpty == false {
                    params[name!] = value
                }
            }
            return params
        }
    }
    private func openMyURl(url : URL){
        let query = url.query
        let params = self.getQueryParameters(query: query)
        if let webPageUrlString = params["webPage"] {
            let vc = UIStoryboard.getWebViewController()
            vc.urlString = webPageUrlString
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(vc, animated: true)
        }
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        print(url)
        if url.absoluteString.contains("abstractors://") {
            self.openMyURl(url: url)
            
        } else {
            return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        }
        return true
    }
  
}
