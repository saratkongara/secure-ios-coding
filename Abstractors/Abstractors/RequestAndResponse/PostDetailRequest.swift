//
//  PostDetailRequest.swift
//  Abstractors
//
//  Created by Anand Kumar on 03/09/2019.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit
import Gloss

class PostDetailRequest : Glossy {
    
    init() {}
    
    required init?(json: JSON) {
    }
    
    func toJSON() -> JSON? {
        return jsonify([])
    }
    
    func getPostDetail(postId: Int, _ completionHandler: @escaping (PostDetailResponse?, Error?) -> Swift.Void) {
        let urlString : String = APIConstants.APIEndPoint + APIConstants.postSuffix + "\(postId)"
        let url : URL = URL(string:urlString)!
        NetworkManger().getData(apiEndpoint: url, withHeaderParams: GeneralUtils.getHeaderValue()){ (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                completionHandler(nil, error)
            }
            else {
                // Read the JSON
                let jsonString = NSString(data:data!, encoding: String.Encoding.utf8.rawValue)
                debugPrint(jsonString!)
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    let jsonDict:PostDetailResponse = PostDetailResponse(json: response!)!
                    completionHandler(jsonDict, error)
                }
                catch {
                    print(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
    
}
