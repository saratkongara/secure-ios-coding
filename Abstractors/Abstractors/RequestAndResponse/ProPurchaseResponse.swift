//
//  ProPurchaseResponse.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss

class ProPurchaseResponse : Glossy {
    
    var success : String?
    var uuid : String?
    
    init() {}
    required init?(json: JSON) {
        self.success = "success" <~~ json
        self.uuid = "uuid" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "success" ~~> self.success,
            "uuid" ~~> self.uuid
            ])
    }
}
