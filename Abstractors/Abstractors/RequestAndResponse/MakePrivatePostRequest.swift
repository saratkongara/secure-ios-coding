//
//  MakePrivatePostRequest.swift
//  Abstractors
//
//  Created by Anand Kumar on 01/09/2019.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit
import Gloss

class MakePrivatePostRequest : Glossy {
    
    var status : String?
    
    init() {}
    
    required init?(json: JSON) {
        self.status = "status" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "status" ~~> self.status,
            ])
    }
    
    func makePrivatePost(postId:String, _ completionHandler: @escaping (MakePrivatePostResponse?, Error?) -> Swift.Void) {
        
        let jsonData = self.toJSON()
        
        let urlString : String = APIConstants.APIEndPoint + APIConstants.makePrivateSuffix + postId
        let url : URL = URL(string:urlString)!
        NetworkManger().postData(withParameters: jsonData!, apiEndpoint: url, withHeaderParams: GeneralUtils.getHeaderValue()){ (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                completionHandler(nil, error)
            }
            else {
                // Read the JSON
                let jsonString = NSString(data:data!, encoding: String.Encoding.utf8.rawValue)
                debugPrint(jsonString!)
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    let jsonDict:MakePrivatePostResponse = MakePrivatePostResponse(json: response!)!
                    completionHandler(jsonDict, error)
                }
                catch {
                    print(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
    
}
