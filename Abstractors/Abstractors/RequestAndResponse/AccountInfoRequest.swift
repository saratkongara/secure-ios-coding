//
//  LoginRequest.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss
class AccountInfoRequest : Glossy {
    init() {}
    required init?(json: JSON) {
        
    }
    func toJSON() -> JSON? {
        return jsonify([])
    }
    func getAccountDetails(_ completionHandler: @escaping (AccountInfoResponse?, Error?) -> Swift.Void){
        let urlString : String = APIConstants.APIEndPoint + APIConstants.accountInfoSuffix
        let url : URL = URL(string:urlString)!
        NetworkManger().getData(apiEndpoint: url, withHeaderParams: GeneralUtils.getHeaderValue()) { (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                completionHandler(nil, error)
            }
            else {
                // Read the JSON
                let jsonString = NSString(data:data!, encoding: String.Encoding.utf8.rawValue)
                debugPrint(jsonString!)
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    let jsonDict:AccountInfoResponse = AccountInfoResponse(json: response!)!
                    GeneralUtils.setIsProUser(isPro: jsonDict.isPro ?? false)
                    if let username = jsonDict.username {
                        GeneralUtils.setUserName(name: username)
                    }
                    if let uuid = jsonDict.card_uuid{
                        GeneralUtils.setUUIDForCard(string: uuid)
                    }
                    completionHandler(jsonDict, error)
                }
                catch {
                    print(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
    
}
