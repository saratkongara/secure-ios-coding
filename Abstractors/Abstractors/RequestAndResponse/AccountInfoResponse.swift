//
//  AccountInfoResponse.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss

class AccountInfoResponse : Glossy {
    var bio : String?
    var email : String?
    var profile_pic : String?
    var username : String?
    var isPro : Bool?
    var card_uuid : String?
    
    init() {}
    required init?(json: JSON) {
        self.bio = "bio" <~~ json
        self.email = "email" <~~ json
        self.profile_pic = "profile_pic" <~~ json
        self.username = "username" <~~ json
        self.isPro = "is_pro" <~~ json
        self.card_uuid = "card_uuid" <~~ json
        if self.isPro == nil{
            self.isPro = "user_type" <~~ json
        }
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
        "bio" ~~> self.bio,
            "email" ~~> self.email,
            "profile_pic" ~~> self.profile_pic,
            "username" ~~> self.username,
            "is_pro" ~~> self.isPro,
            "card_uuid" ~~> self.card_uuid
            ])
    }
}
