//
//  ProPurchaseRequest.swift
//  Abstractors
//
//  Created by ENCIPHERS
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss
class ProPurchaseRequest : Glossy {
    
    var card_number : String? // -  16 digits
    var card_expiration : String? // - Format - MM/YY
    var card_name : String? // -
    var card_cvv : String? // - 3 digits


    init() {}
    required init?(json: JSON) {
        self.card_number = "card_number" <~~ json
        self.card_expiration = "card_expiration" <~~ json
        self.card_name = "card_name" <~~ json
        self.card_cvv = "card_cvv" <~~ json
    }
    func toJSON() -> JSON? {
        return jsonify([
            "card_number" ~~> self.card_number,
            "card_expiration" ~~> self.card_expiration,
            "card_name" ~~> self.card_name,
            "card_cvv" ~~> self.card_cvv
            ])
    }
    func purchasePro(_ completionHandler: @escaping (ProPurchaseResponse?, Error?) -> Swift.Void){
        
        
        let jsonData = self.toJSON()
        
        let urlString : String = APIConstants.APIEndPoint + APIConstants.purchaseSuffix
        let url : URL = URL(string:urlString)!
        NetworkManger().postData(withParameters: jsonData!, apiEndpoint: url, withHeaderParams: GeneralUtils.getHeaderValue()){ (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                completionHandler(nil, error)
            }
            else {
                // Read the JSON
                let jsonString = NSString(data:data!, encoding: String.Encoding.utf8.rawValue)
                debugPrint(jsonString!)
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    let jsonDict:ProPurchaseResponse = ProPurchaseResponse(json: response!)!
                    if let uuid = jsonDict.uuid {
                        GeneralUtils.setUUIDForCard(string: uuid)
                    }
                    completionHandler(jsonDict, error)
                }
                catch {
                    print(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
    
}
