//
//  LoginRequest.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss


class PostRequest : Glossy {
    
    var title : String?
    var content : String?
    var image_url : String?

    init() {}
    required init?(json: JSON) {
        self.title = "title" <~~ json
        self.content = "content" <~~ json
        self.image_url = "image_url" <~~ json
    }
    func toJSON() -> JSON? {
        return jsonify([
            "title" ~~> self.title,
            "content" ~~> self.content,
            "image_url" ~~> self.image_url
            ])
    }
    func newpost(_ completionHandler: @escaping (PostResponse?, Error?) -> Swift.Void){
        let jsonData = self.toJSON()
        
        let urlString : String = APIConstants.APIEndPoint + APIConstants.newPostSuffix
        let url : URL = URL(string:urlString)!
        NetworkManger().postData(withParameters: jsonData!, apiEndpoint: url, withHeaderParams: GeneralUtils.getHeaderValue()){ (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                completionHandler(nil, error)
            }
            else {
                // Read the JSON
                let jsonString = NSString(data:data!, encoding: String.Encoding.utf8.rawValue)
                debugPrint(jsonString!)
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    let jsonDict:PostResponse = PostResponse(json: response!)!
                    completionHandler(jsonDict, error)
                }
                catch {
                    print(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
    
}
