//
//  PostDetailResponse.swift
//  Abstractors
//
//  Created by Anand Kumar on 03/09/2019.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit
import Gloss

class PostDetailResponse : Glossy {
    
    var post : Feed?
    var message: String?
    
    required init?(json: JSON) {
        self.post = "post" <~~ json
        self.message = "message" <~~ json

    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "post" ~~> self.post,
            "message" ~~> self.message
            ])
    }
}
