//
//  CardDetailsResponse.swift
//  Abstractors
//
//  Created by ENCIPHERS
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss

class CardDetailsResponse : Glossy {
    
    var card_name : String?
    var card_no : String?
    var cvv : Int?
    var expiration : String?
    var success : String?
    
    init() {}
    required init?(json: JSON) {
        self.card_name = "card_name" <~~ json
        self.card_no = "card_no" <~~ json
        self.cvv = "cvv" <~~ json
        self.expiration = "expiration" <~~ json
        self.success = "success" <~~ json
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "card_name" ~~> self.card_name,
            "card_no" ~~> self.card_no,
            "cvv" ~~> self.cvv,
            "expiration" ~~> self.expiration,
            "success" ~~> self.success
            ])
    }
}
