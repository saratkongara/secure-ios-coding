//
//  LoginRequest.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss
class HomeFeedRequest : Glossy {
    var isLatest : Bool = false
    var isPrivate : Bool = false
    var username : String?
    init() {}
    required init?(json: JSON) {}
    func toJSON() -> JSON? {
        return jsonify([])
    }
    func getFeeds(_ completionHandler: @escaping (HomeFeedResponse?, Error?) -> Swift.Void){
        var urlString : String
        var url : URL
        if self.username == nil{
            if isLatest {
                urlString = APIConstants.APIEndPoint + APIConstants.homeLatestFeedsSuffix
            } else if isPrivate {
                urlString = APIConstants.APIEndPoint + APIConstants.privateSuffix
            } else {
                urlString = APIConstants.APIEndPoint + APIConstants.homeFeedsSuffix
            }
            
            url = URL(string:urlString)!
        } else{
            urlString = APIConstants.APIEndPoint + APIConstants.userSpecificFeeds
            urlString = urlString + (username!)
            url = URL(string:urlString)!
        }
        NetworkManger().getData(apiEndpoint: url, withHeaderParams: GeneralUtils.getHeaderValue()) { (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                completionHandler(nil, error)
            }
            else {
                // Read the JSON
                let jsonString = NSString(data:data!, encoding: String.Encoding.utf8.rawValue)
                debugPrint(jsonString!)
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    let jsonDict:HomeFeedResponse = HomeFeedResponse(json: response!)!
                    completionHandler(jsonDict, error)
                }
                catch {
                    print(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
    
}
