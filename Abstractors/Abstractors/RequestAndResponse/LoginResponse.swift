//
//  LoginResponse.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss

class LoginResponse : Glossy {
    
    var token : String?
    var isPro : Bool?
    var uuid : String?
    init() {}
    required init?(json: JSON) {
        self.token = "token" <~~ json
        self.isPro = "is_pro" <~~ json
        self.uuid = "uuid" <~~ json
        if self.isPro == nil{
            self.isPro = "user_type" <~~ json
        }
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "uuid" ~~> self.uuid,
            "token" ~~> self.token,
            "is_pro" ~~> self.isPro
            ])
    }
}
