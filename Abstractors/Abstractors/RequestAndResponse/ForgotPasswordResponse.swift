//
//  ForgotPasswordResponse.swift
//  Abstractors
//
//  Created by ENCIPHERS
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss

class ForgotPasswordResponse : Glossy {
    
    var message : String?
    init() {}
    required init?(json: JSON) {
        self.message = "message" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
        "message" ~~> self.message
            ])
    }
}
