//
//  AllUserResponse.swift
//  Abstractors
//
//  Created by Anand Kumar on 01/09/2019.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit
import Gloss

class AllUserResponse : Glossy {
    
    var users : [User]?
    var message : String?

    init() {}
    
    required init?(json: JSON) {
        self.users = "users" <~~ json
        self.message = "message" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "users" ~~> self.users,
            "message" ~~> self.message
            ])
    }
}


class User : Glossy {
    
    var username : String?
    var is_pro : Bool?
    var is_following : String?
    var followers : Int?
    var profile_image : String?
    
    required init?(json: JSON) {
        self.username = "username" <~~ json
        self.is_pro = "is_pro" <~~ json
        self.is_following = "is_following" <~~ json
        self.followers = "followers" <~~ json
        self.profile_image = "profile_image" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "username" ~~> self.username,
            "is_pro" ~~> self.is_pro,
            "is_following" ~~> self.is_following,
            "followers" ~~> self.followers,
            "profile_image" ~~> self.profile_image,
            ])
    }
}
