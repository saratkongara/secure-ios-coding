//
//  LoginRequest.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss
class ForgotPasswordRequest : Glossy {
    
    var email : String?
    var password : String?

    init() {}
    required init?(json: JSON) {
        self.email = "email" <~~ json
    }
    func toJSON() -> JSON? {
        return jsonify([
            "email" ~~> self.email
            ])
    }
    func resetPassword(_ completionHandler: @escaping (ForgotPasswordResponse?, Error?) -> Swift.Void){
        
        
        let jsonData = self.toJSON()
        
        let urlString : String = APIConstants.APIEndPoint + APIConstants.forgotPasswordSuffix
        let url : URL = URL(string:urlString)!
        NetworkManger().postData(withParameters: jsonData!, apiEndpoint: url, withHeaderParams: GeneralUtils.getHeaderValue()){ (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                completionHandler(nil, error)
            }
            else {
                // Read the JSON
                let jsonString = NSString(data:data!, encoding: String.Encoding.utf8.rawValue)
                debugPrint(jsonString!)
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    let jsonDict:ForgotPasswordResponse = ForgotPasswordResponse(json: response!)!
                    completionHandler(jsonDict, error)
                }
                catch {
                    print(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
    
}
