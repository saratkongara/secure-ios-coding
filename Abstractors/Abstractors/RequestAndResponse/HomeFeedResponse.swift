//
//  HomeFeedResponse.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss

class HomeFeedResponse : Glossy {
    var message : String?
    var posts : [Feed]?
    init() {}
    required init?(json: JSON) {
        self.posts = "posts" <~~ json
        self.message = "message" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
        "posts" ~~> self.posts,
        "message" ~~> self.message
            ])
    }
}
