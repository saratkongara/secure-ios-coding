//
//  SignUpResponse.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss

class SignUpResponse : Glossy {
    
    var success : String?
    init() {}
    required init?(json: JSON) {
        self.success = "success" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
        "success" ~~> self.success
            ])
    }
}
