//
//  LoginRequest.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss
class SignUpRequest : Glossy {
    var username : String?
    var email : String?
    var password : String?

    init() {}
    required init?(json: JSON) {
        self.email = "email" <~~ json
        self.password = "password" <~~ json
        self.username = "username" <~~ json
    }
    func toJSON() -> JSON? {
        return jsonify([
            "email" ~~> self.email,
            "password" ~~> self.password,
            "username" ~~> self.username
            ])
    }
    func signUpApp(_ completionHandler: @escaping (SignUpResponse?, Error?) -> Swift.Void){
        
        
        let jsonData = self.toJSON()
        GeneralUtils.printDictionary(dictionary: jsonData!)
        let urlString : String = APIConstants.APIEndPoint + APIConstants.signupSufix
        let url : URL = URL(string:urlString)!
        NetworkManger().postData(withParameters: jsonData!, apiEndpoint: url, withHeaderParams: GeneralUtils.getHeaderValue()){ (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                completionHandler(nil, error)
            }
            else {
                // Read the JSON
                let jsonString = NSString(data:data!, encoding: String.Encoding.utf8.rawValue)
                debugPrint(jsonString!)
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    let jsonDict:SignUpResponse = SignUpResponse(json: response!)!
                    completionHandler(jsonDict, error)
                }
                catch {
                    print(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
    
}
