//
//  FollowUserRequest.swift
//  Abstractors
//
//  Created by Anand Kumar on 01/09/2019.
//  Copyright © 2019 Harry. All rights reserved.
//

import Foundation
import Gloss

class FollowUserRequest : Glossy {
    
    var username : String?
    
    init() {}
    
    required init?(json: JSON) {
        self.username = "username" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "username" ~~> self.username,
            ])
    }
    
    func followUser(_ follow: Bool, _ completionHandler: @escaping (FollowUserResponse?, Error?) -> Swift.Void) {
        let urlString : String = APIConstants.APIEndPoint + (follow ? APIConstants.followSuffix : APIConstants.unfollowSuffix ) + (username ?? "")
        let url : URL = URL(string:urlString)!
        NetworkManger().getData(apiEndpoint: url, withHeaderParams: GeneralUtils.getHeaderValue()) { (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                completionHandler(nil, error)
            } else {
                // Read the JSON
                let jsonString = NSString(data:data!, encoding: String.Encoding.utf8.rawValue)
                debugPrint(jsonString!)
                do {
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    let jsonDict:FollowUserResponse = FollowUserResponse(json: response!)!
                    completionHandler(jsonDict, error)
                } catch {
                    print(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
    
}
