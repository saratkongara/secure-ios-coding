//
//  FollowUserResponse.swift
//  Abstractors
//
//  Created by Anand Kumar on 01/09/2019.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit
import Gloss

class FollowUserResponse : Glossy {
    
    var success : String?

    init() {}
    
    required init?(json: JSON) {
        self.success = "success" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "success" ~~> self.success,
            ])
    }
}
