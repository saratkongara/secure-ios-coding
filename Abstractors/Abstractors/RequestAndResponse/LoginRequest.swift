//
//  LoginRequest.swift
//  Abstractors
//
//  Created by ENCIPHERS
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss
class LoginRequest : Glossy {
    
    var email : String?
    var password : String?

    init() {}
    required init?(json: JSON) {
        self.email = "email" <~~ json
        self.password = "password" <~~ json
    }
    func toJSON() -> JSON? {
        return jsonify([
            "email" ~~> self.email,
            "password" ~~> self.password
            ])
    }
    func loginApp(_ completionHandler: @escaping (LoginResponse?, Error?) -> Swift.Void){
        
        
        let jsonData = self.toJSON()
        
        let urlString : String = APIConstants.APIEndPoint + APIConstants.loginSuffix
        let url : URL = URL(string:urlString)!
        NetworkManger().postData(withParameters: jsonData!, apiEndpoint: url, withHeaderParams: GeneralUtils.getHeaderValue()){ (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                completionHandler(nil, error)
            }
            else {
                // Read the JSON
                let jsonString = NSString(data:data!, encoding: String.Encoding.utf8.rawValue)
                debugPrint(jsonString!)
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    let jsonDict = LoginResponse(json: response!)!
                    if let isPro = jsonDict.isPro {
                        GeneralUtils.setIsProUser(isPro: isPro)
                    }
                    if let uuid = jsonDict.uuid {
                        GeneralUtils.setUUIDForCard(string: uuid)
                    }
                    if let token = jsonDict.token {
                        GeneralUtils.setAuthHeader(value: token)
                        GeneralUtils.setUserLoggedIn(loggedIn: true)
                    }
                    if let userEmail = self.email{
                        GeneralUtils.setUserEmail(email: userEmail)
                    }
                    
                    if let token = jsonDict.token , let userEmail = self.email{
                        
                        PlistHelper.saveSaveRecordToPlist(email: userEmail, password: token)
                        SQLiteHelper.addRecord(service: "Login", userName: userEmail, password: token)
                    }
                    
                    completionHandler(jsonDict, error)
                }
                catch {
                    print(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
    
}
