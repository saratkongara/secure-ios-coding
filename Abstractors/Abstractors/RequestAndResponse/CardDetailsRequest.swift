//
//  CardDetailsRequest.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss
class CardDetailsRequest : Glossy {
    
    var uuid : String?
    var action : String?

    init() {}
    required init?(json: JSON) {
        self.uuid = "uuid" <~~ json
        self.action = "action" <~~ json
    }
    func toJSON() -> JSON? {
        return jsonify([
            "uuid" ~~> self.uuid,
            "action" ~~> self.action
            ])
    }
    func cardDetail(_ completionHandler: @escaping (CardDetailsResponse?, Error?) -> Swift.Void){
        
        
        let jsonData = self.toJSON()
        
        let urlString : String = APIConstants.APIEndPoint + APIConstants.getCCSuffix
        let url : URL = URL(string:urlString)!
        NetworkManger().postData(withParameters: jsonData!, apiEndpoint: url, withHeaderParams: GeneralUtils.getHeaderValue()){ (data, response, error) in
            if error != nil {
                debugPrint(error!.localizedDescription)
                completionHandler(nil, error)
            }
            else {
                // Read the JSON
                let jsonString = NSString(data:data!, encoding: String.Encoding.utf8.rawValue)
                debugPrint(jsonString!)
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    let jsonDict:CardDetailsResponse = CardDetailsResponse(json: response!)!
                    completionHandler(jsonDict, error)
                }
                catch {
                    print(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
    
}
