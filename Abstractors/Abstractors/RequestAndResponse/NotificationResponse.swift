//
//  NotificationResponse.swift
//  Abstractors
//
//  Created by Anand Kumar on 02/09/2019.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit
import Gloss

class NotificationResponse : Glossy {
    
    var success : String?
    var feeds: [Feed]?
    
    init() {}
    
    required init?(json: JSON) {
        self.success = "success" <~~ json
        self.feeds = "posts" <~~ json
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "success" ~~> self.success,
            "posts" ~~> self.feeds,
            ])
    }
}
