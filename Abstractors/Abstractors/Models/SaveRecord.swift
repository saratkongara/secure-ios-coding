//
//  Record.swift
//  SqliteIntegration
//
//  Created by Ayush Gupta on 1/20/19.
//  Copyright © 2019 Ayush Gupta. All rights reserved.
//

import Foundation

class SaveRecord {
    var service: String
    var userName: String
    var password: String
    
    init(service: String, userName: String, password: String) {
        self.service = service
        self.userName = userName
        self.password = password
    }
}
