//
//  Feed.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import Gloss

class Feed : Glossy{
    
    var author : String?
    var content : String?
    var date_posted : Date?
    var id : Int?
    var post_image : String?
    var profile_image : String?
    var status : String?
    var title : String?
    
    
    
    required init?(json: JSON) {
        self.author = "author" <~~ json
        self.content = "content" <~~ json
        if let dateString =  json["date_posted"] as? String{
            self.date_posted = dateString.getServerFormattedDate()
        }
        self.id = "id" <~~ json
        self.post_image = "post_image" <~~ json
        self.profile_image = "profile_image" <~~ json
        self.status = "status" <~~ json
        self.title = "title" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "author" ~~> self.author,
            "content" ~~> self.content,
            "date_posted" ~~> self.date_posted!.toServerDateString(),
            "id" ~~> self.id,
            "post_image" ~~> self.post_image,
            "profile_image" ~~> self.profile_image,
            "status" ~~> self.status,
            "title" ~~> self.title,
            ])
    }
}
