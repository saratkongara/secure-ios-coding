//
//  AbsConstants.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation

let keyForgetPasswordEnabled        = "ForgetPasswordEnabled"
let keyJailBreakEnabled             = "JailBreakEnabled"
let keyTouchIdEnabled               = "TouchIdEnabled"
let keySSLEnabled                   = "SSLEnabled"
let keySaveToKeychain               = "SaveToKeychain"
let keyBlurScreenOnBackground       = "BlurScreenOnBackground"

struct ConfigurationKeys {
    static let applicationDidBecomeActive      = "applicationDidBecomeActive"
    static let applicationWillResignActive     = "applicationWillResignActive"

    static var ForgetPasswordEnabled: Bool {
        get {
            return UserDefaults.standard.bool(forKey: keyForgetPasswordEnabled)
        } set {
            return UserDefaults.standard.set(newValue, forKey: keyForgetPasswordEnabled)
        }
    }
    static var JailBreakEnabled: Bool{
        get {
            return UserDefaults.standard.bool(forKey: keyJailBreakEnabled)
        } set {
            return UserDefaults.standard.set(newValue, forKey: keyJailBreakEnabled)
        }
    }
    static var TouchIdEnabled: Bool{
        get {
            return true//UserDefaults.standard.bool(forKey: keyTouchIdEnabled)
        } set {
            return UserDefaults.standard.set(newValue, forKey: keyTouchIdEnabled)
        }
    }
    static var SSLEnabled: Bool{
        get {
            return UserDefaults.standard.bool(forKey: keySSLEnabled)
        } set {
            return UserDefaults.standard.set(newValue, forKey: keySSLEnabled)
        }
    }
    static var saveToKeychain: Bool{
        get {
            return true//UserDefaults.standard.bool(forKey: keySaveToKeychain)
        } set {
            return UserDefaults.standard.set(newValue, forKey: keySaveToKeychain)
        }
    }
    static var blurScreenOnBackground: Bool{
        get {
            return true//UserDefaults.standard.bool(forKey: keyBlurScreenOnBackground)
        } set {
            return UserDefaults.standard.set(newValue, forKey: keyBlurScreenOnBackground)
        }
    }
}

struct APIConstants
{
    
    static let APIEndPoint              = "https://tw.the-abstractors.com"
    static let HostName                 = "tw.the-abstractors"
    static let forgotPasswordUrl        = "https://the-abstractors.com/reset_password"
//    static let loginViaTwitterSuffix    = "/api/logintwitter"
    static let FBCFBundleURLSchemes     = "fb583517738779263"
    static let FacebookId               = "583517738779263"
    static let signupSufix              = "/api/sign-up"
    static let accountInfoSuffix        = "/api/account"
    static let forgotPasswordSuffix     = "/api/forgot"
    static let homeLatestFeedsSuffix    = "/api/latest"
    static let getCCSuffix              = "/api/get_cc"
    static let loginSuffix              = "/api/login"
    static let followSuffix             = "/api/follow/"
    static let unfollowSuffix           = "/api/unfollow/"
    static let usersSuffix              = "/api/users"
    static let privateSuffix            = "/api/private"
    static let makePrivateSuffix        = "/api/make_private/"
    static let userSpecificFeeds        = "/api/user/"
    static let otherHost                = "127.0.0.1"
    static let homeFeedsSuffix          = "/api/home"
    static let newPostSuffix            = "/api/new"
    static let purchaseSuffix           = "/api/pro"
    static let notificationSuffix       = "/api/notify"
    static let postSuffix               = "/api/post/"
    static let TBR                      = "Y29vbGRldkBkZXYuY29tfENvb2xAMTIzIA=="
    //For more info check: Internal-s3-abstractors-data.s3.amazonaws.com
}
struct StringConstants {
    static let twitterConsumerSecret    = "Ds9izZCpTPZsKKdVXCliDxEGFAAhDhye6qleA6awDwOeyIKxV0"
    static let twitterConsumerKey       = "D2oxocrpyybjlXzleULK42MOT"
    static let AuthHeaderKey            = "AutherisationHeaderValue"
    static let AuthorizationKey         = "Authorization"
    static let PostRequest              = "POST"
    static let unknown                  = "unknown"
    static let GetRequest               = "GET"
    static let BioMetricSecKey          = "AbsBioMetricSecKey"
}
