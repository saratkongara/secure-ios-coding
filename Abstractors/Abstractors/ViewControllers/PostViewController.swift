//
//  LoginViewViewController.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import UIKit
import MBProgressHUD
import FacebookCore


class PostViewController: UIViewController {

    @IBOutlet weak var txtFTitle: UITextField!
    @IBOutlet weak var txtViewMessage: UITextView!
    
    @IBAction func btnPostClicked(_ sender: UIButton?) {
        AppEvents.logEvent(AppEvents.Name(rawValue: "btnPostClicked"))
        if self.txtFTitle.text == nil || (self.txtFTitle.text?.count) ?? 0 == 0{
            UIAlertController.showAlert(with: "Title can't be empty", on: self)
            return
        }
        if self.txtViewMessage.text == nil || (self.txtViewMessage.text?.count) ?? 0 == 0{
            UIAlertController.showAlert(with: "Meesage can't be empty", on: self)
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let request = PostRequest()
        request.title = self.txtFTitle.text
        request.content = self.txtViewMessage.text
        request.newpost { (response, error) in
            DispatchQueue.main.async {
                if error != nil{UIAlertController.showAlert(with: error!, on: self)}
                else{
                    if response != nil{
                        self.navigationController?.popViewController(animated: true)
                        return
                    }
                    UIAlertController.showAlert(with: "Error in posing new content, please contact adminstrator", on: self)
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTapGesture()
        self.txtViewMessage.layer.borderColor = UIColor.lightGray.cgColor
        self.txtViewMessage.layer.borderWidth = 1.0
    }
    @objc func done(){
        self.view.endEditing(true)
    }
    func setTapGesture()
    {
        let tapges = UITapGestureRecognizer(target: self, action: #selector(PostViewController.done))
        tapges.numberOfTapsRequired = 1
        tapges.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapges)
    }


}
extension PostViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

