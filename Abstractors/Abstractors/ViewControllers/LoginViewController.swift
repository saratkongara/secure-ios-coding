//
//  LoginViewViewController.swift
//  Abstractors
//
//  Created by ENCIPHERS on 04/03/19.
//  Copyright © 2019 . All rights reserved.
//

import UIKit
import TwitterKit
import MBProgressHUD
import LocalAuthentication
import FacebookCore

private let configurationPassword = "1234"

class LoginViewController: UIViewController {

    @IBOutlet weak var txtFEmail: UITextField!
    @IBOutlet weak var txtFPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLoginWithTwitter: UIButton!
    @IBOutlet weak var btnSignUpNow: UIButton!
    @IBOutlet weak var btnUseTouchId: UIButton!
    @IBOutlet weak var imgViewLogo: UIImageView!
    
    var tapCount : Int = 0
    
    @IBAction func btnTouchIdClicked(_ sender: Any) {
    }
    
    @IBAction func btnLoginClicked(_ sender: Any?) {
        AppEvents.logEvent(AppEvents.Name(rawValue: "btnLoginClicked"))
        let email = self.txtFEmail.text
        let password = self.txtFPassword.text
        if (email?.count) ?? 0 == 0 && GeneralUtils.isValidEmail(testStr: email) == false{
            UIAlertController.showAlert(with: "Please enter valid email address", on: self)
            return
        }
        if (password?.count) ?? 0 == 0{
            UIAlertController.showAlert(with: "Please enter password", on: self)
            return
        }
        self.login(email: email ?? "", and: password ?? "" )
        
    }
    @IBAction func btnForgotPasswordClicked(_ sender: Any?) {
        let vc = UIStoryboard.getWebViewController()
        vc.urlString = APIConstants.forgotPasswordUrl
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnLoginWithTwitterClicked(_ sender: Any?) {
    }
    
    @IBAction func btnSignUpNowClicked(_ sender: Any?) {
        
        let vc = UIStoryboard.getSignUpViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //Life Cycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkForTouchId()
        self.checkForTwiterLogin()
        self.checkForForgotPassword()
        //self.setUpTapGesture()
        self.txtFEmail.delegate = self
        self.txtFPassword.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.deregisterFromKeyboardNotifications()
    }
    //Other Functions
    func setUpTapGesture(){
        let tapGes = UITapGestureRecognizer(target: self, action: #selector(handleTaponLogo))
        tapGes.numberOfTapsRequired = 1
        tapGes.numberOfTouchesRequired = 1
        self.imgViewLogo.addGestureRecognizer(tapGes)
    }
    @objc func handleTaponLogo(){
        if self.tapCount >= 5{
            self.getAppInfoAlert()
            self.tapCount = 0
        } else{ self.tapCount = self.tapCount + 1}
    }
    func getAppInfoAlert(){
        var detailString = "App Version : 1.0\n"
        if ConfigurationKeys.SSLEnabled {detailString = detailString + "SSL Enabled\n"}
        if ConfigurationKeys.ForgetPasswordEnabled {detailString = detailString + "ForgotPassword Enabled\n"}
        if ConfigurationKeys.JailBreakEnabled {detailString = detailString + "JailBreak Enabled\n"}
        if ConfigurationKeys.TouchIdEnabled {detailString = detailString + "TouchIdEnabled Enabled\n"}
        if ConfigurationKeys.saveToKeychain {detailString = detailString + "saveToKeychain Enabled\n"}
        if ConfigurationKeys.blurScreenOnBackground {detailString = detailString + "blurScreenOnBackground Enabled\n"}
        UIAlertController.showTextInputAlert(with: "\(detailString)\nEnter Pin to update configuration", on: self) { pin in
            if pin == configurationPassword {
                let vc = UIStoryboard.getAbsConstantsViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    func checkForForgotPassword(){
        self.btnForgotPassword.isHidden = true
        if ConfigurationKeys.ForgetPasswordEnabled{
            self.btnForgotPassword.isHidden = false
        }
    }
    func checkForTwiterLogin(){
        self.btnLoginWithTwitter.isHidden = true

    }
    func checkForTouchId(){
        self.btnUseTouchId.isHidden = true
    }
    func login(email : String, and password : String){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let request = LoginRequest()
        request.email = email
        request.password = password
        request.loginApp { (response, error) in
            DispatchQueue.main.async {
                AccountInfoRequest().getAccountDetails({ (_, _) in })
                MBProgressHUD.hide(for: self.view, animated: true)
                if error != nil{UIAlertController.showAlert(with: error!, on: self)}
                else{
                    if (response?.token) != nil {
                        GeneralUtils.setUserEmail(email: (self.txtFEmail.text)!)
                        let vc = UIStoryboard.getFeedsViewController()
                        self.navigationController?.setViewControllers([vc], animated: true)
                        return
                    }
                    UIAlertController.showAlert(with: "Error in Login", on: self)
                }
            }
            
        }
    }
}
extension LoginViewController {
    @objc func keyboardWasShown(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame = CGRect(x: 0, y: -(keyboardSize.height/4), width: self.view.frame.width, height: self.view.frame.height)
        }
    }
    @objc func keyboardWillBeHidden(_ notification: Notification) {
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
    }
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWasShown), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    func deregisterFromKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}
extension LoginViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
