//
//  LoginViewViewController.swift
//  Abstractors
//
//  Created by ENCIPHERS
//  Copyright © 2019 . All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage

@objc protocol PostActionDelegate: NSObjectProtocol {
    func openPostDetails(_ postId: Int)
    func openUserPosts(_ username: String)
    @objc optional func makePostPublic(_ postId: Int, makePublic: Bool)
    @objc optional func sharePost(_ postId: Int)
}

class FeedDetailCell : UITableViewCell {
    
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var lblPostDetail: UILabel!
    @IBOutlet weak var lblPostTitle: UILabel!
    @IBOutlet weak var lblDatePosted: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var ImageViewHeightConstraint: NSLayoutConstraint!
    weak var delegate: PostActionDelegate?
    
    var feed: Feed?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        imgViewProfile.image = UIImage(named: "accountPlaceHolder")
        imgViewProfile.contentMode = .scaleAspectFit
        imgViewProfile.layer.cornerRadius = imgViewProfile.frame.width/2
        imgViewProfile.layer.masksToBounds = true
        imgViewProfile.layer.shadowOffset = CGSize(width: 5, height: 5)
        imgViewProfile.layer.shadowColor = UIColor.black.cgColor
        lblUserName.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openUserPosts)))
        lblPostTitle.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openPostDetails)))
    }
    
    func reset() {
        self.lblPostDetail.text = ""
        self.lblPostTitle.text = ""
        self.lblDatePosted.text = ""
        self.lblUserName.text = ""
        self.imgViewProfile.image = nil
    }
    
    func setViewModel(_ feed:Feed, delegate: PostActionDelegate, isUserNameClickable: Bool = true, isTitleClickable: Bool = true) {
        reset()
        self.feed = feed
        self.delegate = delegate
        lblUserName.text = feed.author
        lblPostTitle.text = feed.title
        lblDatePosted.text = feed.date_posted?.getDisplayDate()
        lblPostDetail.text = feed.content
        lblUserName.isUserInteractionEnabled = isUserNameClickable
        lblPostTitle.isUserInteractionEnabled = isTitleClickable
        if let post_image = feed.post_image, post_image.count >= 20 {
            ImageViewHeightConstraint.constant = 190
            imgPost.sd_setImage(with: URL(string: post_image)) { (image, error, cache, url) in }
        } else {
            ImageViewHeightConstraint.constant = 0
        }
        if let image = feed.profile_image, !image.isEmpty {
            imgViewProfile.sd_setImage(with: URL(string: image)) { (image, error, cache, url) in
                if image == nil { self.imgViewProfile.image = UIImage(named: "accountPlaceHolder") }
            }
        }
        layoutSubviews()
    }
    
    @objc func openPostDetails() {
        delegate?.openPostDetails(feed?.id ?? 0)
    }
    
    @objc func openUserPosts() {
        delegate?.openUserPosts(feed?.author ?? "")
    }
}

class FeedsViewController: UIViewController {
    
    @IBOutlet weak var tableViewObj: UITableView!
    var arrDataSource : [Feed] = []
    var userName : String?
    var isLatestPosts : Bool = false
    var isPrivatePosts : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.userName != nil{ self.title = self.userName} else {
            self.title = "Home"
            if self.isLatestPosts {
                self.title = "Latest Posts"
            } else if self.isPrivatePosts {
                self.title = "Private Posts"
            }
        }
        self.setMoreButton()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getNewFeeds()
        self.addProButton()
    }
    func addProButton(){
        self.navigationItem.leftBarButtonItem = nil
        if !GeneralUtils.isProUser(){return}
        if self.userName != nil{return}
        if self.isLatestPosts{return}
        if self.isPrivatePosts {return}
        let pro = UIBarButtonItem(title: "PRO", style: UIBarButtonItem.Style.done, target: self, action: #selector(FeedsViewController.btnProClicked))
        self.navigationItem.leftBarButtonItem = pro
    }
    @objc func btnProClicked(){
        UIAlertController.showAlert(with: "You are a pro user", on: self)
    }
    @objc func selectOptions()
    {
        let optionMenu = UIAlertController(title: nil, message: "Abstractor", preferredStyle: .actionSheet)
        let gopro = UIAlertAction(title: "Pro", style: UIAlertAction.Style.default) { (_) in
            if GeneralUtils.isProUser() == false{
                let vc = UIStoryboard.getProUserPaymentViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = UIStoryboard.getProUserAccountViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        let newpost = UIAlertAction(title: "New Post", style: UIAlertAction.Style.default) { (_) in
            let vc = UIStoryboard.getPostViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let account = UIAlertAction(title: "Account", style: UIAlertAction.Style.default) { (_) in
            let vc = UIStoryboard.getAccountViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let logout = UIAlertAction(title: "Logout", style: UIAlertAction.Style.default) { (_) in
            GeneralUtils.logout()
            let vc = UIStoryboard.getLoginViewController()
            self.navigationController?.setViewControllers([vc], animated: true)
        }
        let latestpost = UIAlertAction(title: "Latest Posts", style: UIAlertAction.Style.default) { (_) in
            let vc = UIStoryboard.getFeedsViewController()
            vc.isLatestPosts = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let privatepost = UIAlertAction(title: "Private Posts", style: UIAlertAction.Style.default) { (_) in
            let vc = UIStoryboard.getFeedsViewController()
            vc.isPrivatePosts = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let users = UIAlertAction(title: "Users", style: UIAlertAction.Style.default) { (_) in
            let vc = UIStoryboard.getUsersViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let notifications = UIAlertAction(title: "Notifications", style: UIAlertAction.Style.default) { (_) in
            let vc = UIStoryboard.getNotificationsViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (_) in
            self.dismiss(animated: true, completion: nil)
        }
        optionMenu.addAction(gopro)
        optionMenu.addAction(latestpost)
        optionMenu.addAction(privatepost)
        optionMenu.addAction(users)
        optionMenu.addAction(notifications)
        optionMenu.addAction(newpost)
        optionMenu.addAction(account)
        optionMenu.addAction(logout)
        optionMenu.addAction(cancel)
        
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.barButtonItem = self.navigationItem.rightBarButtonItem
        }
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func setMoreButton() {
        let testUIBarButtonItem = UIBarButtonItem(image: UIImage(named: "btnMore"), style: .plain, target: self, action: #selector(selectOptions))
        self.navigationItem.rightBarButtonItem  = testUIBarButtonItem
    }

    func getNewFeeds()
    {
        let request = HomeFeedRequest()
        request.isLatest = self.isLatestPosts
        request.isPrivate = self.isPrivatePosts
        if self.userName != nil && (self.userName?.count) ?? 0 > 0{
            request.username = userName
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        request.getFeeds { (response, error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.arrDataSource.removeAll()
                if let response = response {
                    if let posts = response.posts {
                        for feed in posts {
                            self.arrDataSource.append(feed)
                            self.tableViewObj.delegate = self
                            self.tableViewObj.dataSource = self
                            self.tableViewObj.reloadData()
                        }
                    }
                    if response.message?.lowercased() == "token is invalid" {
                        GeneralUtils.logout()
                        let vc = UIStoryboard.getLoginViewController()
                        self.navigationController?.setViewControllers([vc], animated: true)
                    }
                }
            }
        }
    }
}

extension FeedsViewController: PostActionDelegate {
    func openPostDetails(_ postId: Int) {
        let vc = UIStoryboard.getPostDetailViewController()
        vc.postId = postId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openUserPosts(_ username: String) {
        let feedsvc = UIStoryboard.getFeedsViewController()
        feedsvc.userName = username
        self.navigationController?.pushViewController(feedsvc, animated: true)
    }
}

extension FeedsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedDetailCell", for: indexPath) as! FeedDetailCell
        let feed = self.arrDataSource[indexPath.row]
        cell.setViewModel(feed, delegate: self, isUserNameClickable: self.userName == nil)
        return cell
    }
}
