//
//  LoginViewViewController.swift
//  Abstractors
//
//  Created by ENCIPHERS
//  Copyright © 2019 . All rights reserved.
//

import UIKit
import MBProgressHUD



class AccountViewController: UIViewController {

    @IBOutlet weak var lblTopUserName: UILabel!
    @IBOutlet weak var lblTopEmail: UILabel!
    @IBOutlet weak var lblTopBio: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var lblBottomUserName: UILabel!
    @IBOutlet weak var lblBottomEmail: UILabel!
    @IBOutlet weak var lblBttomBio: UILabel!
    
    @IBOutlet weak var lblTwitterName: UILabel!
    
    @IBOutlet weak var imgViewProfile: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getAccountDetails()
        
    }
    func getAccountDetails()  {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        AccountInfoRequest().getAccountDetails { (response, error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                if error != nil{UIAlertController.showAlert(with: error!, on: self)}
                else{
                    if response != nil{
                        self.lblTopBio.text = response?.bio
                        self.lblBttomBio.text = response?.bio
                        
                        self.lblTopUserName.text = response?.username
                        self.lblBottomUserName.text = response?.username
                        
                        self.lblTopEmail.text = response?.email
                        self.lblBottomEmail.text = response?.email
                        
                        if let name = GeneralUtils.getTwiterName(){
                            self.lblTwitterName.text = name
                        } else {
                            self.lblTwitterName.text = "No Associated Account"
                        }
                        
                        if GeneralUtils.isProUser(){
                            self.lblTwitterName.text = "PRO"
                            self.imgLogo.image = UIImage(named: "prologo")
                        }
                        return
                    }
                    UIAlertController.showAlert(with: "Error in retriving account detail, contact adminstrator", on: self)
                }
            }
        }
    }
}
