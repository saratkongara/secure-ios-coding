//
//  LoginViewViewController.swift
//  Abstractors
//
//  Created by ENCIPHERS
//  Copyright © 2019. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var txtFEmail: UITextField!
    
    @IBAction func btnRequestNewPasswordClcked(_ sender: UIButton?) {
        if GeneralUtils.isValidEmail(testStr: self.txtFEmail.text){
            let request = ForgotPasswordRequest()
            request.email = self.txtFEmail.text
            request.resetPassword { (response, error) in
                DispatchQueue.main.async {
                    if error != nil{UIAlertController.showAlert(with: error!, on: self)}
                    else{
                        if response != nil{
                            if let token = response?.message{
                                self.navigationController?.popToRootViewController(animated: true)
                                return
                            }
                        }
                        UIAlertController.showAlert(with: "Error in sending reset link please retry later", on: self)
                    }
                }
            }
        }
        else{
            UIAlertController.showAlert(with: "Please enter valid email", on: self)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
}
