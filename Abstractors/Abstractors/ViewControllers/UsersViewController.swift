//
//  UsersViewController.swift
//  Abstractors
//
//  Created by Anand Kumar on 01/09/2019.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage

private let pageSize = 10

protocol UserDetailCellDelegate {
    func clickedOnFollow(_ model: User?, index: Int, isFollow: Bool)
    func openUserPosts(_ username: String)
}

class UserDetailCell : UITableViewCell {
    
    @IBOutlet weak var proLabel: UILabel!
    @IBOutlet weak var followerLabel: UILabel!
    @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    var user: User?
    var index = 0
    var delegate: UserDetailCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgViewProfile.image = UIImage(named: "accountPlaceHolder")
        imgViewProfile.contentMode = .scaleAspectFit
        imgViewProfile.layer.cornerRadius = imgViewProfile.frame.width/2
        imgViewProfile.layer.masksToBounds = true
        imgViewProfile.layer.shadowOffset = CGSize(width: 5, height: 5)
        imgViewProfile.layer.shadowColor = UIColor.black.cgColor
        userNameLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openUserPosts)))
        userNameLabel.isUserInteractionEnabled = true
        selectionStyle = .none
    }
    
    func setViewModel(_ user: User, index:Int, delegate: UserDetailCellDelegate) {
        self.user = user
        self.index = index
        self.delegate = delegate
        proLabel.isHidden = !(user.is_pro ?? false)
        followerLabel.text = "No of followers \(user.followers ?? 0)"
        followButton.setTitle(user.is_following == "False" ? "Follow" : "Unfollow" , for: .normal)
        userNameLabel.text = user.username
        if let image = user.profile_image, !image.isEmpty {
            imgViewProfile.sd_setImage(with: URL(string: image)) { (image, error, cache, url) in
                if image == nil { self.imgViewProfile.image = UIImage(named: "accountPlaceHolder") }
            }
        }
    }
    
    @IBAction func didClickOnFollow(_ sender: Any) {
        delegate?.clickedOnFollow(user, index: index, isFollow: user?.is_following == "False")
    }
    
    @objc func openUserPosts() {
        delegate?.openUserPosts(user?.username ?? "")
    }
}

class UsersViewController: UIViewController {
    
    @IBOutlet weak var tableViewObj: UITableView!
    var allUser: [User] = []
    var arrDataSource : [User] = []
    @IBOutlet weak var pageNumberLabel: UILabel!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    var totalPages: Int = 0
    var pageNumber = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Users"
        self.tableViewObj.rowHeight = 80
        pageNumberLabel.isHidden = true
        prevButton.isHidden = true
        nextButton.isHidden = true
        getUsers()
    }
    
    func getUsers() {
        let request = AllUserRequest()
        MBProgressHUD.showAdded(to: self.view, animated: true)
        request.allUser { (response, error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.allUser.removeAll()
                if let response = response {
                    if let users = response.users?.reversed() {
                        for post in users {
                            self.allUser.append(post)
                        }
                        self.pageNumber = 1
                        if self.allUser.count % pageSize == 0 {
                            self.totalPages = self.allUser.count / pageSize
                        } else {
                            self.totalPages = self.allUser.count / pageSize + 1
                        }
                        self.updatePageNumber()
                    }
                    if response.message?.lowercased() == "token is invalid" {
                        GeneralUtils.logout()
                        let vc = UIStoryboard.getLoginViewController()
                        self.navigationController?.setViewControllers([vc], animated: true)
                    }
                }
            }
        }
    }
    
    func updatePageNumber() {
        self.pageNumberLabel.isHidden = false
        self.pageNumberLabel.text = "\(pageNumber) of \(totalPages)"
        var startIndex = 0, endIndex = 0
        if pageNumber == 1 {
            startIndex = 0
            endIndex = pageSize - 1
            self.prevButton.isHidden = true
            self.nextButton.isHidden = false
        } else if pageNumber == totalPages {
            startIndex = (pageNumber - 1)*pageSize
            endIndex = self.allUser.count - 1
            self.prevButton.isHidden = false
            self.nextButton.isHidden = true
        } else {
            startIndex = (pageNumber - 1)*pageSize
            endIndex = pageNumber*pageSize - 1
            self.prevButton.isHidden = false
            self.nextButton.isHidden = false
        }
        self.arrDataSource.removeAll()
        for index in startIndex...endIndex {
            self.arrDataSource.append(self.allUser[index])
        }
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        self.tableViewObj.reloadData()
    }
    
    @IBAction func prevTapped(_ sender: Any) {
        if pageNumber == 1 { return }
        pageNumber = pageNumber - 1
        updatePageNumber()
    }
    @IBAction func nextTapped(_ sender: Any) {
        if pageNumber == totalPages { return }
        pageNumber = pageNumber + 1
        updatePageNumber()
    }
    
}

extension UsersViewController: UserDetailCellDelegate {
    func clickedOnFollow(_ user: User?, index: Int, isFollow: Bool) {
        if let user = user {
            let request = FollowUserRequest()
            request.username = user.username
            MBProgressHUD.showAdded(to: self.view, animated: true)
            request.followUser(isFollow) { (response, error) in
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let response = response, response.success == "Success" {
                        user.is_following = isFollow ? "True" : "False"
                        if let followers = user.followers {
                            user.followers = isFollow ? followers + 1 : followers - 1
                        }
                        self.arrDataSource[index] = user
                        self.tableViewObj.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    } else {
                        if user.username == GeneralUtils.getUserName() {
                            UIAlertController.showAlert(with: "You can't follow yourself", on: self)
                        } else {
                            UIAlertController.showAlert(with: "Unable to \(isFollow ? "Follow" : "Unfollow") user \(user.username != nil ? user.username! : "")", on: self)
                        }                        
                    }
                }
            }
        }
    }
    
    func openUserPosts(_ username: String) {
        let feedsvc = UIStoryboard.getFeedsViewController()
        feedsvc.userName = username
        self.navigationController?.pushViewController(feedsvc, animated: true)
    }
}

extension UsersViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserDetailCell", for: indexPath) as! UserDetailCell
        let user = self.arrDataSource[indexPath.row]
        cell.setViewModel(user, index: indexPath.row, delegate: self)
        return cell
    }
}
