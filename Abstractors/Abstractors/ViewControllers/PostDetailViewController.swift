//
//  PostDetailViewController.swift
//  Abstractors
//
//  Created by Anand Kumar on 03/09/2019.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage

class PostDetailCell : UITableViewCell {
    
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var lblPostDetail: UILabel!
    @IBOutlet weak var lblPostTitle: UILabel!
    @IBOutlet weak var lblDatePosted: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var makePublicButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var ImageViewHeightConstraint: NSLayoutConstraint!
    weak var delegate: PostActionDelegate?
    
    var feed: Feed?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        imgViewProfile.image = UIImage(named: "accountPlaceHolder")
        imgViewProfile.contentMode = .scaleAspectFit
        imgViewProfile.layer.cornerRadius = imgViewProfile.frame.width/2
        imgViewProfile.layer.masksToBounds = true
        imgViewProfile.layer.shadowOffset = CGSize(width: 5, height: 5)
        imgViewProfile.layer.shadowColor = UIColor.black.cgColor
        lblUserName.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openUserPosts)))
        lblPostTitle.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openPostDetails)))
        makePublicButton.isHidden = true
    }
    
    func reset() {
        self.lblPostDetail.text = ""
        self.lblPostTitle.text = ""
        self.lblDatePosted.text = ""
        self.lblUserName.text = ""
        self.imgViewProfile.image = nil
    }
    
    func setViewModel(_ feed:Feed, delegate: PostActionDelegate, isUserNameClickable: Bool = true, isTitleClickable: Bool = true) {
        reset()
        self.feed = feed
        self.delegate = delegate
        lblUserName.text = feed.author
        lblPostTitle.text = feed.title
        lblDatePosted.text = feed.date_posted?.getDisplayDate()
        lblPostDetail.text = feed.content
        lblUserName.isUserInteractionEnabled = isUserNameClickable
        lblPostTitle.isUserInteractionEnabled = isTitleClickable
        if GeneralUtils.getUserName() == feed.author {
            makePublicButton.isHidden = false
            makePublicButton.isEnabled = true
            makePublicButton.setTitle(feed.status == "public" ? "Make Private" : "Make Public", for: .normal)
        } else {
            makePublicButton.isHidden = true
            makePublicButton.isEnabled = false
        }
        if let post_image = feed.post_image, post_image.count >= 20 {
            ImageViewHeightConstraint.constant = 190
            imgPost.sd_setImage(with: URL(string: post_image)) { (image, error, cache, url) in }
        } else {
            ImageViewHeightConstraint.constant = 0
        }
        if let image = feed.profile_image, !image.isEmpty {
            imgViewProfile.sd_setImage(with: URL(string: image)) { (image, error, cache, url) in
                if image == nil { self.imgViewProfile.image = UIImage(named: "accountPlaceHolder") }
            }
        }
        layoutSubviews()
    }
    
    @objc func openPostDetails() {
        delegate?.openPostDetails(feed?.id ?? 0)
    }
    
    @objc func openUserPosts() {
        delegate?.openUserPosts(feed?.author ?? "")
    }
    
    @IBAction func didClickOnMakePublic(_ sender: Any) {
        delegate?.makePostPublic?(feed?.id ?? 0, makePublic: feed?.status != "public")
    }
    
    @IBAction func didClickOnShare(_ sender: Any) {
        delegate?.sharePost?(feed?.id ?? 0)
    }
}

class PostDetailViewController: UIViewController {
    
    @IBOutlet weak var tableViewObj: UITableView!
    
    var arrDataSource : [Feed] = []
    var postId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Detail"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getNewFeeds()
    }
    func getNewFeeds() {
        guard let postId = postId else {
            return
        }
        let request = PostDetailRequest()
        MBProgressHUD.showAdded(to: self.view, animated: true)
        request.getPostDetail(postId: postId) { (response, error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.arrDataSource.removeAll()
                if let response = response {
                    if let post = response.post {
                        self.arrDataSource.append(post)
                        self.tableViewObj.delegate = self
                        self.tableViewObj.dataSource = self
                        self.tableViewObj.reloadData()
                    }
                    if response.message?.lowercased() == "token is invalid" {
                        GeneralUtils.logout()
                        let vc = UIStoryboard.getLoginViewController()
                        self.navigationController?.setViewControllers([vc], animated: true)
                    }
                }
            }
        }
    }
}

extension PostDetailViewController: PostActionDelegate {
    func openPostDetails(_ postId: Int) {
        let vc = UIStoryboard.getPostDetailViewController()
        vc.postId = postId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openUserPosts(_ username: String) {
        let feedsvc = UIStoryboard.getFeedsViewController()
        feedsvc.userName = username
        self.navigationController?.pushViewController(feedsvc, animated: true)
    }
    
    func makePostPublic(_ postId: Int, makePublic: Bool) {
        let request = MakePrivatePostRequest()
        request.status = makePublic ? "False" : "True"
        MBProgressHUD.showAdded(to: self.view, animated: true)
        request.makePrivatePost(postId: "\(postId)") { (response, error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                if error != nil { UIAlertController.showAlert(with: error!, on: self) }
                else if response?.success == "Success" {
                    if let post = self.arrDataSource.first {
                        post.status = makePublic ? "public" : "private"
                        self.arrDataSource = [post]
                        self.tableViewObj.reloadData()
                    }
                }
            }
        }
    }
    
    func sharePost(_ postId: Int) {
        let textToShare = "https://the-abstractors.com/post/\(postId)"
        let activityVC = UIActivityViewController (activityItems: [textToShare], applicationActivities: nil)
        activityVC.excludedActivityTypes = [ .print, .assignToContact, .saveToCameraRoll]
        self.present(activityVC, animated: true)
    }
}

extension PostDetailViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostDetailCell", for: indexPath) as! PostDetailCell
        cell.reset()
        let feed = self.arrDataSource[indexPath.row]
        cell.setViewModel(feed, delegate: self, isTitleClickable: false)
        return cell
    }
}
