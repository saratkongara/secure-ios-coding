//
//  NotificationsViewController.swift
//  Abstractors
//
//  Created by Anand Kumar on 03/09/2019.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage

class NotificationsCell : UITableViewCell {
    
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    var feed: Feed?
    weak var delegate: PostActionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openPostDetails)))
        titleLabel.isUserInteractionEnabled = true
        authorLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openUserPosts)))
        authorLabel.isUserInteractionEnabled = true

        self.selectionStyle = .none
    }
    
    func setViewModel(_ feed: Feed, index:Int, delegate: PostActionDelegate) {
        self.feed = feed
        self.delegate = delegate
        authorLabel.text = feed.author
        dateLabel.text = feed.date_posted?.getDisplayDate()
        titleLabel.text = feed.title
        descriptionLabel.text = feed.content
    }
    
    @objc func openPostDetails() {
        delegate?.openPostDetails(feed?.id ?? 0)
    }
    
    @objc func openUserPosts() {
        delegate?.openUserPosts(feed?.author ?? "")
    }
}

class NotificationsViewController: UIViewController {
    
    @IBOutlet weak var tableViewObj: UITableView!
    var arrDataSource : [Feed] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Notifications"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getNotifications()
    }
    
    func getNotifications() {
        let request = NotificationRequest()
        MBProgressHUD.showAdded(to: self.view, animated: true)
        request.getNotifications { (response, error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.arrDataSource.removeAll()
                if let response = response {
                    if let feeds = response.feeds {
                        for feed in feeds {
                            self.arrDataSource.append(feed)
                            self.tableViewObj.delegate = self
                            self.tableViewObj.dataSource = self
                            self.tableViewObj.reloadData()
                        }
                    }
                    if response.success?.lowercased() == "token is invalid" {
                        GeneralUtils.logout()
                        let vc = UIStoryboard.getLoginViewController()
                        self.navigationController?.setViewControllers([vc], animated: true)
                    }
                }
            }
        }
    }
    
}

extension NotificationsViewController: PostActionDelegate {
    func openPostDetails(_ postId: Int) {
        let vc = UIStoryboard.getPostDetailViewController()
        vc.postId = postId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openUserPosts(_ username: String) {
        let feedsvc = UIStoryboard.getFeedsViewController()
        feedsvc.userName = username
        self.navigationController?.pushViewController(feedsvc, animated: true)
    }
}

extension NotificationsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsCell", for: indexPath) as! NotificationsCell
        let feed = self.arrDataSource[indexPath.row]
        cell.setViewModel(feed, index: indexPath.row, delegate: self)
        return cell
    }
}
