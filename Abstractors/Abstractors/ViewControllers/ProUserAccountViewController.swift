//
//  ProUserAccountViewController.swift
//  Abstractors
//
//  Created by ENCIPHERS
//  Copyright © 2019 . All rights reserved.
//

import UIKit
import MBProgressHUD


class ProUserAccountViewController: UIViewController {
   
    static let imageOneURLString        = "https://s3.ap-south-1.amazonaws.com/pro-img-abstractors/ales-krivec-107499-unsplash.jpg"
    static let imageTwoURLString        = "https://s3.ap-south-1.amazonaws.com/pro-img-abstractors/artur-pokusin-740-unsplash.jpg"
    static let imageThreeURLString      = "https://s3.ap-south-1.amazonaws.com/pro-img-abstractors/austin-neill-160132-unsplash.jpg"
    static let imageFourURLString       = "https://s3.ap-south-1.amazonaws.com/pro-img-abstractors/benjamin-davies-332625-unsplash.jpg"
    static let imageFiveURLString       = "https://s3.ap-south-1.amazonaws.com/pro-img-abstractors/bryan-minear-319945-unsplash.jpg"
    static let imageSixURLString        = "https://s3.ap-south-1.amazonaws.com/pro-img-abstractors/christian-joudrey-98245-unsplash.jpg"
    
    @IBOutlet weak var viewImgContainer: UIView!
    @IBOutlet weak var lblHeaderTitle   : UILabel!
    @IBOutlet weak var lblSubTitle      : UILabel!
    @IBOutlet weak var btnHome          : UIButton!
    
    @IBOutlet weak var imgViewOne       : UIImageView!
    @IBOutlet weak var imgViewTwo       : UIImageView!
    @IBOutlet weak var imgViewThree     : UIImageView!
    @IBOutlet weak var imgViewFour      : UIImageView!
    @IBOutlet weak var imgViewFive      : UIImageView!
    @IBOutlet weak var imgViewSix       : UIImageView!
    
    @IBOutlet weak var imgViewHeightConstraint: NSLayoutConstraint!
    
    @IBAction func btnCardDetailClicked(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let request = CardDetailsRequest()
        request.uuid = GeneralUtils.getUUIDForCard()
        request.cardDetail { (response, error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                if response != nil{
                    if let cardno = response?.card_no{
                        var detailString = "Card Number :- " + cardno
                        detailString = detailString + "\nName On Card :- "
                        detailString = detailString + ((response?.card_name) ?? "")
                        detailString = detailString + "\nExpiry :- "
                        detailString = detailString + ((response?.expiration) ?? "")
                        detailString = detailString + "\nCVV :- "
                        detailString = detailString + String((response?.cvv) ?? 0)
                        UIAlertController.showAlert(with: detailString, on: self)
                    }
                }
            }
            
        }
        
    }
    
    @IBAction func btnRemoveCardClicked(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let request = CardDetailsRequest()
        request.uuid = GeneralUtils.getUUIDForCard()
        request.action = "remove"
        request.cardDetail { (response, error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                if response != nil{
                    if ((response?.success) ?? "").lowercased() != "success"{
                        UIAlertController.showAlert(with: "Error in removing card", on: self)
                    } else {
                        GeneralUtils.setIsProUser(isPro: false)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    @IBAction func btnHomeClicked(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.imgViewHeightConstraint.constant = self.viewImgContainer.frame.height/2
        self.view.layoutSubviews()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpImages()
        
    }
    
    func setUpImages(){
        self.setImageUrlAndStartLoading(    imageView: self.imgViewOne,     urlString: ProUserAccountViewController.imageOneURLString   )
        self.setImageUrlAndStartLoading(    imageView: self.imgViewTwo,     urlString: ProUserAccountViewController.imageTwoURLString   )
        self.setImageUrlAndStartLoading(    imageView: self.imgViewThree,   urlString: ProUserAccountViewController.imageThreeURLString )
        self.setImageUrlAndStartLoading(    imageView: self.imgViewFour,    urlString: ProUserAccountViewController.imageFourURLString  )
        self.setImageUrlAndStartLoading(    imageView: self.imgViewFive,    urlString: ProUserAccountViewController.imageFiveURLString  )
        self.setImageUrlAndStartLoading(    imageView: self.imgViewSix,     urlString: ProUserAccountViewController.imageSixURLString   )
    }

    func setImageUrlAndStartLoading(imageView : UIImageView , urlString : String){
        imageView.sd_setImage(with: URL(string: urlString)) { (image, error, cache, url) in
            
        }
    }
    
}
