//
//  LoginViewViewController.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import UIKit
import MBProgressHUD
import FacebookCore

class SignUpViewController: UIViewController {

    var selectedIndex : Int = 1
    @IBOutlet weak var txtfUserName: UITextField!
    @IBOutlet weak var txtfEmail: UITextField!
    @IBOutlet weak var txtfPassword: UITextField!
    @IBOutlet weak var txtfConfirmPassword: UITextField!
    
    
    
    @IBAction func btnSignUpClicked(_ sender: UIButton?) {
        AppEvents.logEvent(AppEvents.Name(rawValue: "btnSignUpClicked"))
       

        if (self.txtfUserName.text?.count) ?? 0 == 0{
            UIAlertController.showAlert(with: "Please Enter User Name", on: self)
            return
        }
        if GeneralUtils.isValidEmail(testStr: self.txtfEmail.text) == false{
            UIAlertController.showAlert(with: "Please Enter Valid Email", on: self)
            return
        }
        if GeneralUtils.isPasswordMatchAndIsValid(password: self.txtfPassword.text, andConfirmPassword: self.txtfConfirmPassword.text) == false{
            UIAlertController.showAlert(with: "Please enter matching password with minimum lenght of 6 characters.", on: self)
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let request = SignUpRequest()
        request.username = self.txtfUserName.text
        request.email = self.txtfEmail.text
        request.password = self.txtfPassword.text
        request.signUpApp { (response, error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                if error != nil{
                    UIAlertController.showAlert(with: error!, on: self)
                }
                else{
                    if response != nil{
                        if response?.success != nil
                        {
                            if response!.success!.lowercased() == "success"
                            {
                                self.navigationController?.popToRootViewController(animated: true)
                                return
                            }
                        }
                    }
                }
                UIAlertController.showAlert(with: "Something went wrong please try again", on: self)
            }
            
        }
    }
    @IBAction func btnSignInClicked(_ sender: UIButton?) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtfUserName.delegate = self
        self.txtfEmail.delegate = self
        self.txtfPassword.delegate = self
        self.txtfConfirmPassword.delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.deregisterFromKeyboardNotifications()
    }
}
extension SignUpViewController {
    @objc func keyboardWasShown(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame = CGRect(x: 0, y: -(keyboardSize.height/CGFloat(self.selectedIndex)), width: self.view.frame.width, height: self.view.frame.height)
        }
    }
    @objc func keyboardWillBeHidden(_ notification: Notification) {
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
    }
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWasShown), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func deregisterFromKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
}
extension SignUpViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var tag = textField.tag
        tag = tag - 1
        switch tag {
        case 4:
            self.txtfUserName.becomeFirstResponder()
        case 3:
            self.txtfEmail.becomeFirstResponder()
        case 2:
            self.txtfPassword.becomeFirstResponder()
        case 1:
            self.txtfConfirmPassword.becomeFirstResponder()
        default:
            self.view.endEditing(true)
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        self.selectedIndex = textField.tag
        return true
    }
}
