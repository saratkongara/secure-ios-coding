//
//  LocalLoginViewController.swift
//  Abstractors
//
//  Created by Anand Kumar on 06/09/2019.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit
import MBProgressHUD
import LocalAuthentication


class LocalLoginViewController: UIViewController {
    
    @IBOutlet weak var loggedInLabel: UILabel!
    
    //Life Cycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        if let userEmail = GeneralUtils.getUserEmail() {
            loggedInLabel.text = "Logged in as : \(userEmail)"
        }
    }
    
    //Actions
    @IBAction func btnTouchIdClicked(_ sender: Any) {
        let myContext = LAContext()
        let myLocalizedReasonString = "Login with Biometric Id"
        var authError: NSError?
        if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                DispatchQueue.main.async {
                    if success {
                        if let _ = GeneralUtils.getAuthHeader(){
                            let vc = UIStoryboard.getFeedsViewController()
                            self.navigationController?.setViewControllers([vc], animated: true)
                        } else{
                            self.btnlogoutClicked(nil)
                        }
                       
                    } else { UIAlertController.showAlert(with: "Not able to authenticate", on: self)}
                }
            }
        } else {
            UIAlertController.showAlert(with: "No authentication available please check settings", on: self)
        }
    }
    
    @IBAction func btnlogoutClicked(_ sender: Any?) {
        GeneralUtils.logout()
        let vc = UIStoryboard.getLoginViewController()
        self.navigationController?.setViewControllers([vc], animated: true)
    }
}
