//
//  AbsConstantsViewController.swift
//  Abstractors
//
//  Created by Anand Kumar on 01/07/19.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit

protocol AbsConstantsCellDelegate: class {
    func toggleValue(index:Int, value: Bool)
}

class AbsConstantsCell : UITableViewCell {
    
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var keySwitch: UISwitch!
    weak var delegate: AbsConstantsCellDelegate?
    var index = 0
    
    func setKey(_ key:String, delegate:AbsConstantsCellDelegate, index:Int) {
        keyLabel.text = key
        keySwitch.isOn = UserDefaults.standard.bool(forKey: key)
        self.delegate = delegate
        self.index = index
    }
    
    @IBAction func toggleTapped(_ sender: Any) {
        delegate?.toggleValue(index: index, value: keySwitch.isOn)
    }
    
}

class AbsConstantsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AbsConstantsCellDelegate {
    
    var dataSource = [keyForgetPasswordEnabled, keyJailBreakEnabled, keyTouchIdEnabled, keySSLEnabled, keySaveToKeychain, keyBlurScreenOnBackground].map({[$0: UserDefaults.standard.bool(forKey: $0)]})

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Abs Constants"
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AbsConstantsCell", for: indexPath) as! AbsConstantsCell
        cell.setKey(dataSource[indexPath.row].first?.key ?? "", delegate: self, index: indexPath.row)
        return cell
    }
    
    
    func toggleValue(index: Int, value: Bool) {
        print("toggleValue \(index)")
        dataSource[index].updateValue(value, forKey: dataSource[index].first?.key ?? "")
    }

    @IBAction func saveTapped(_ sender: Any) {
        for data in dataSource {
            UserDefaults.standard.set(data.first?.value ?? false, forKey: data.first?.key ?? "")
        }
        UserDefaults.standard.synchronize()
        exit(0);
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
