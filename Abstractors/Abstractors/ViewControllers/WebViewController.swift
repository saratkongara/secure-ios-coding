//
//  WebViewController.swift
//  Abstractors
//
//  Created by Anand Kumar on 28/05/19.
//  Copyright © 2019 Harry. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKUIDelegate {
    
    var wKWebView: WKWebView?
    
    var urlString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wKWebView = WKWebView(frame: view.frame)
        wKWebView?.uiDelegate = self
        wKWebView?.frame = view.bounds
        wKWebView?.configuration.preferences.javaScriptEnabled = true
        wKWebView?.configuration.preferences.javaScriptCanOpenWindowsAutomatically = true
        wKWebView?.load(URLRequest(url: URL(string: urlString)!))
        view.addSubview(wKWebView!)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in
            completionHandler()
        }))
        present(alert, animated: true) {}
    }
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: .cancel, handler: { _ in
            completionHandler(true)
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { _ in
            completionHandler(false)
        }))
        present(alert, animated: true) {}
    }
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        let alert = UIAlertController(title: prompt, message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = defaultText
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            completionHandler(alert?.textFields?.first?.text)
        }))
        present(alert, animated: true) {}
    }
}
