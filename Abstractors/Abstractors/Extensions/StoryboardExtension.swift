//
//  StoryboardExtension.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import UIKit
extension UIStoryboard {
    fileprivate static let main = UIStoryboard(name: "Main", bundle: nil)
    
    
    func getViewController<T: UIViewController>() -> T {
        return instantiateViewController(withIdentifier: T.storyboardID) as! T
    }
    
    class func getLoginViewController() -> LoginViewController {
        return main.getViewController()
    }
    class func getAccountViewController() -> AccountViewController {
        return main.getViewController()
    }
    class func getFeedsViewController() -> FeedsViewController {
        return main.getViewController()
    }
    class func getForgetPasswordViewController() -> ForgotPasswordViewController {
        return main.getViewController()
    }
    class func getPostViewController() -> PostViewController {
        return main.getViewController()
    }
    class func getSignUpViewController() -> SignUpViewController {
        return main.getViewController()
    }
    class func getProUserPaymentViewController() -> ProUserPaymentViewController {
        return main.getViewController()
    }
    class func getProUserAccountViewController() -> ProUserAccountViewController {
        return main.getViewController()
    }
    
    class func getWebViewController() -> WebViewController {
        return main.getViewController()
    }
    
    class func getAbsConstantsViewController() -> AbsConstantsViewController {
        return main.getViewController()
    }
    
    class func getUsersViewController() -> UsersViewController {
        return main.getViewController()
    }
    
    class func getNotificationsViewController() -> NotificationsViewController {
        return main.getViewController()
    }
    
    class func getPostDetailViewController() -> PostDetailViewController {
        return main.getViewController()
    }
    
    class func getLocalLoginViewController() -> LocalLoginViewController {
        return main.getViewController()
    }
}
