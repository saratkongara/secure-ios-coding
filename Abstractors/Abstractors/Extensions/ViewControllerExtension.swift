//
//  ViewControllerExtension.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import UIKit
extension UIViewController {
    static var storyboardID: String {
        return className
    }
}
