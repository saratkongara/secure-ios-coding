//
//  StringExtension.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
extension String
{
    public func getServerFormattedDate() -> Date?{
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss ZZ"
        if let date = formatter.date(from: self){return date}
        return nil
    }
}
