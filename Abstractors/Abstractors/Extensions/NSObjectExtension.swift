//
//  NSObjectExtension.swift
//  Abstractors
//
//  Created by ENCIPHERS.
//  Copyright © 2019 . All rights reserved.
//

import Foundation

extension NSObject {
    var className: String {
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }
    
    static var className: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}
