//
//  KeychainService.swift
//  Abstractors
//
//  Created by ENCIPHERS
//  Copyright © 2019 . All rights reserved.
//

import SwiftKeychainWrapper

private let keyPassword = "password"
private let keyIsProUser = "isProUser"
private let keyUserName = "userName"
private let keyTwitterName = "twitterName"
private let keyAuthHeader = "authHeader"
private let keyUserEmail = "userEmail"
private let keyUUIDForCard = "uuidForCard"

class KeychainService: NSObject {
    
    var service: String = ""
    var account: String = ""
    
    convenience init(service: String, account:String) {
        self.init()
        self.service = service
        self.account = account
    }
    
    var password: String? {
        get {
            return KeychainWrapper.standard.string(forKey: service + account + keyPassword )
        } set {
            if let newValue = newValue {
                _ = KeychainWrapper.standard.set(newValue, forKey: service + account + keyPassword )
            } else {
                _ = KeychainWrapper.standard.removeObject(forKey: service + account + keyPassword )
            }
        }
    }
    
    var isProUser: Bool {
        get {
            return KeychainWrapper.standard.bool(forKey: service + account + keyIsProUser ) ?? false
        } set {
            _ = KeychainWrapper.standard.set(newValue, forKey: service + account + keyIsProUser )
        }
    }
    
    var userName: String? {
        get {
            return KeychainWrapper.standard.string(forKey: service + account + keyUserName )
        } set {
            if let newValue = newValue {
                _ = KeychainWrapper.standard.set(newValue, forKey: service + account + keyUserName )
            } else {
                _ = KeychainWrapper.standard.removeObject(forKey: service + account + keyUserName )
            }
        }
    }
    
    var twitterName: String? {
        get {
            return KeychainWrapper.standard.string(forKey: service + account + keyTwitterName )
        } set {
            if let newValue = newValue {
                _ = KeychainWrapper.standard.set(newValue, forKey: service + account + keyTwitterName )
            } else {
                _ = KeychainWrapper.standard.removeObject(forKey: service + account + keyTwitterName )
            }
        }
    }
    
    var authHeader: String? {
        get {
            return KeychainWrapper.standard.string(forKey: service + account + keyAuthHeader)
        } set {
            if let newValue = newValue {
                _ = KeychainWrapper.standard.set(newValue, forKey: service + account + keyAuthHeader )
            } else {
                _ = KeychainWrapper.standard.removeObject(forKey: service + account + keyAuthHeader )
            }
        }
    }
    
    var userEmail: String? {
        get {
            return KeychainWrapper.standard.string(forKey: service + account + keyUserEmail)
        } set {
            if let newValue = newValue {
                _ = KeychainWrapper.standard.set(newValue, forKey: service + account + keyUserEmail )
            } else {
                _ = KeychainWrapper.standard.removeObject(forKey: service + account + keyUserEmail )
            }
        }
    }
    
    var uuidForCard: String? {
        get {
            return KeychainWrapper.standard.string(forKey: service + account + keyUUIDForCard )
        } set {
            if let newValue = newValue {
                _ = KeychainWrapper.standard.set(newValue, forKey: service + account + keyUUIDForCard )
            } else {
                _ = KeychainWrapper.standard.removeObject(forKey: service + account + keyUUIDForCard )
            }
        }
    }
}
