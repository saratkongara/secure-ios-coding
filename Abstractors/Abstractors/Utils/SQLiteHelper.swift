//
//  SQLiteHelper.swift
//  Abstractors
//
//  Created by Mohammad Haroon on 14/11/19.
//  Copyright © 2019 Harry. All rights reserved.
//

import Foundation
class SQLiteHelper{
    static func addRecord(service : String , userName : String, password : String){
        let sqliteDbStore = SqliteDbStore()
        let record = SaveRecord(service: service, userName: userName, password: password)
        sqliteDbStore.create(record: record)
    }
    static func updateRecord(service : String , userName : String, password : String){
        let sqliteDbStore = SqliteDbStore()
        let record = SaveRecord(service: service, userName: userName, password: password)
        sqliteDbStore.update(record: record)
        
    }
    static func RemoveRecord(userName : String){
        let sqliteDbStore = SqliteDbStore()
        sqliteDbStore.delete(userName: userName)
    }
    static func getRecord(userName : String) -> SaveRecord?{
        let sqliteDbStore = SqliteDbStore()
        do {
            let record = try sqliteDbStore.read(userName: userName)
            return record
        } catch {
            return nil
        }
    }
}
