//
//  Plist.swift
//  Abstractors
//
//  Created by Mohammad Haroon on 14/11/19.
//  Copyright © 2019 Harry. All rights reserved.
//

import Foundation
class PlistHelper{
    static func saveSaveRecordToPlist(email : String , password : String ){
        let plistBaseUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let absPlistURL = plistBaseUrl.appendingPathComponent("abs.plist")
        print(absPlistURL.absoluteString)
        var dict = [String : String]()
        dict["email"] = email
        dict["password"] = password
        // Swift Dictionary To Data.
        do  {
        let data = try PropertyListSerialization.data(fromPropertyList: dict, format: PropertyListSerialization.PropertyListFormat.binary, options: 0)
            do {
                try data.write(to: absPlistURL, options: .atomic)
                print("Successfully write")
            }catch (let err){
                print(err.localizedDescription)
            }
        }catch (let err){
            print(err.localizedDescription)
        }
    }
    static func getPasswordFromPlist() -> String?{
        let plistBaseUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let absPlistURL = plistBaseUrl.appendingPathComponent("abs.plist")
        if let dict = NSDictionary(contentsOfFile : absPlistURL.absoluteString){
            return dict["password"] as? String
        }
        return nil
        
    }
}
