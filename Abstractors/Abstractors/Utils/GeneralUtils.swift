//
//  GeneralUtils.swift
//  Abstractors
//
//  Created by ENCIPHERS
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import UIKit

class GeneralUtils {
    static func isProUser() -> Bool{
        if ConfigurationKeys.saveToKeychain {
            return KeychainService(service: "Login", account: "").isProUser
        } else {
            return UserDefaults.standard.bool(forKey: "isProUserKey")
        }
    }
    static func setIsProUser(isPro : Bool){
        if ConfigurationKeys.saveToKeychain {
            KeychainService(service: "Login", account: "").isProUser = isPro
        } else {
            UserDefaults.standard.set(isPro, forKey: "isProUserKey")
            
        }
    }
    
    static func getUserName() -> String?{
        if ConfigurationKeys.saveToKeychain {
            return KeychainService(service: "Login", account: "").userName
        } else {
            return UserDefaults.standard.string(forKey: "userNameKey")
        }
    }
    static func setUserName(name : String?){
        if ConfigurationKeys.saveToKeychain {
            KeychainService(service: "Login", account: "").userName = name
        } else {
            UserDefaults.standard.set(name, forKey: "userNameKey")
        }
    }
    
    static func getTwiterName() -> String?{
        if ConfigurationKeys.saveToKeychain {
            return KeychainService(service: "Login", account: "").twitterName
        } else {
            return UserDefaults.standard.string(forKey: "twiterName")
        }
    }
    static func setTwiterName(name : String){
        if ConfigurationKeys.saveToKeychain {
            KeychainService(service: "Login", account: "").twitterName = name
        } else {
            UserDefaults.standard.setValue(name, forKey: "twiterName")
        }
    }
    
    static func getAuthHeader() -> String?{
        if ConfigurationKeys.saveToKeychain {
            return KeychainService(service: "Login", account: "").authHeader
        } else {
            return UserDefaults.standard.string(forKey: StringConstants.AuthHeaderKey)
        }
    }
    static func setAuthHeader(value : String?){
        if ConfigurationKeys.saveToKeychain {
            KeychainService(service: "Login", account: "").authHeader = value
        } else {
            UserDefaults.standard.set(value, forKey: StringConstants.AuthHeaderKey)
        }
    }
    
    
    static func getUserEmail() -> String?{
        if ConfigurationKeys.saveToKeychain {
            return KeychainService(service: "Login", account: "").userEmail
        } else {
            return UserDefaults.standard.string(forKey: "UserEmailKey")
        }
    }
    static func setUserEmail(email : String?){
        if ConfigurationKeys.saveToKeychain {
            KeychainService(service: "Login", account: "").userEmail = email
        } else {
            UserDefaults.standard.set(email, forKey: "UserEmailKey")
        }
    }
    
    static func getUUIDForCard() -> String? {
        if ConfigurationKeys.saveToKeychain {
            return  KeychainService(service: "Login", account: "").uuidForCard
        } else {
            return UserDefaults.standard.string(forKey: "UserCardUUID")
        }
    }
    static func setUUIDForCard(string : String?){
        if ConfigurationKeys.saveToKeychain {
            KeychainService(service: "Login", account: "").uuidForCard = string
        } else {
            UserDefaults.standard.set(string, forKey: "UserCardUUID")
        }
    }
    
    
    static func isUserLogin() -> Bool{
        return UserDefaults.standard.bool(forKey: "isUserLoggedIn")
    }
    static func setUserLoggedIn(loggedIn : Bool){
        UserDefaults.standard.set(loggedIn, forKey: "isUserLoggedIn")
    }
    static func getHeaderValue() -> Dictionary<String,String> {
        var headerData = Dictionary<String,String>()
        headerData["Content-Type"] = "application/json"
        headerData["Accept"] = "application/json"
        headerData["Accept-Encoding"] = "gzip"
        if GeneralUtils.getAuthHeader()?.count ?? 0 > 0{
            headerData[StringConstants.AuthorizationKey] = GeneralUtils.getAuthHeader()
        }
        return headerData
    }
    static func isValidEmail(testStr:String?) -> Bool {
        if testStr == nil{return false}
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr!)
    }
    static func isPasswordMatchAndIsValid(password:String? , andConfirmPassword : String?) -> Bool {
        if (password?.count) ?? 0 == 0{return false}
        if (andConfirmPassword?.count) ?? 0 == 0{return false}
        if password! != andConfirmPassword!{return false}
        if (password?.count) ?? 0 < 6{return false}
        return true
    }
    static func printDictionary(dictionary:Dictionary<String, Any>){
        do{
            let data = try JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                debugPrint(json)
            }
        }catch{
            
        }
    }
    
    static func logout() {
        GeneralUtils.setUserLoggedIn(loggedIn: false)
        GeneralUtils.setAuthHeader(value: nil)
        GeneralUtils.setIsProUser(isPro: false)
        GeneralUtils.setUUIDForCard(string: nil)
        GeneralUtils.setUserName(name: nil)
        if let userEmail = GeneralUtils.getUserEmail() {
            KeychainService(service: "Login", account: userEmail).password = nil
        }
        GeneralUtils.setUserEmail(email: nil)
    }
}
