//
//  NetworkManager.swift
//  Abstractors
//
//  Created by ENCIPHERS
//  Copyright © 2019 . All rights reserved.
//

import Foundation
class NetworkManger: NSObject {
    
    public var networkSession: URLSession?
    public var networkDataTask: URLSessionDataTask?
    private var retryCount:              Int = 0
    
    public func getSharedConfiguration(_ isEphemeral: Bool) -> URLSessionConfiguration {
        let sessionConfiguration = isEphemeral ? URLSessionConfiguration.ephemeral : URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = 180
        sessionConfiguration.timeoutIntervalForResource = 180
        return sessionConfiguration
    }
    
    public func cancelActiveTask() {
        self.networkDataTask?.cancel()
    }
    public func getNetworkSession() -> URLSession{
       return URLSession(configuration: getSharedConfiguration(false))
    }
    public func getData(apiEndpoint apiURL: URL, withHeaderParams headerParams:[String: String]?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        var request = URLRequest(url: apiURL)
        if headerParams != nil {
            for (key, value) in headerParams! {
                request.setValue(value, forHTTPHeaderField: key)
            }
        }
        request.setValue(GeneralUtils.getAuthHeader(), forHTTPHeaderField: StringConstants.AuthorizationKey)
        request.httpMethod = StringConstants.GetRequest
        if self.networkSession == nil {self.networkSession = self.getNetworkSession()}
        weak var weakSelf = self
        self.retryCount += 1
        self.networkDataTask = self.networkSession?.dataTask(with: request as URLRequest) { (data, response, error) in
            debugPrint("called api with url: \(apiURL)")
            //Check for response code 200
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    var rError = error
                    if rError == nil {
                        rError = NSError(domain: Bundle.main.bundleIdentifier!, code: -99999, userInfo: [NSLocalizedDescriptionKey:"Invalid status code"])
                    }
                    completionHandler(nil, nil, rError)
                    return
                }
            }
            
            if error != nil {
                debugPrint(error!.localizedDescription)
                if ((error!._code == NSURLErrorTimedOut) || (error!._code == NSURLErrorNetworkConnectionLost)) {
                    //Error Case
                    debugPrint(error?.localizedDescription ?? "Error - Please check")
                }
            }
            do {
                if data != nil {
                    let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    if dict != nil {
                        // debugPrint("Data received from Server is \(GeneralUtilities.printDictionary(dictionary: dict!))")
                    }
                }
            } catch {
                debugPrint("Error parsing data received: \(error.localizedDescription)")
            }
            weakSelf?.retryCount = 0
            completionHandler(data, response, error)
        }
        self.networkDataTask?.resume()
    }
    
    public func postData(withParameters params:Dictionary<String , Any>, apiEndpoint apiURL: URL, withHeaderParams headerParams:[String: String]?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        var request = URLRequest(url: apiURL)
        if headerParams != nil {
            for (key, value) in headerParams! {
                request.setValue(value, forHTTPHeaderField: key)
            }
        }
        request.setValue(GeneralUtils.getAuthHeader(), forHTTPHeaderField: StringConstants.AuthorizationKey)
        request.httpMethod = StringConstants.PostRequest
        
        do {
            
            let data = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
            let json = String(data: data, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))?.replacingOccurrences(of: "+", with: "%2B").replacingOccurrences(of: "&", with: "%26")
            if json != nil{
                let postData = json!.data(using: .utf8)
                request.httpBody = postData
            }
        } catch {debugPrint(error.localizedDescription); return}
        
        if self.networkSession == nil { self.networkSession = self.getNetworkSession()}
        weak var weakSelf = self
        self.retryCount += 1
        self.networkDataTask = self.networkSession?.dataTask(with: request as URLRequest) { (data, response, error) in
            debugPrint("called api with url: \(apiURL)")
            //Check for response code 200
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    var rError = error
                    if rError == nil {
                        rError = NSError(domain: Bundle.main.bundleIdentifier!, code: -99999, userInfo: [NSLocalizedDescriptionKey:"Invalid status code"])
                    }
                    completionHandler(nil, nil, rError)
                    return
                }
            }
            
            if error != nil {
                debugPrint(error!.localizedDescription)
                if ((error!._code == NSURLErrorTimedOut) || (error!._code == NSURLErrorNetworkConnectionLost)) {
                    if weakSelf?.retryCount != nil && (weakSelf?.retryCount)! < 1 {
                        debugPrint("Retrying for api: \(apiURL)")
                        weakSelf?.postData(withParameters: params, apiEndpoint: apiURL, withHeaderParams: headerParams, completionHandler: completionHandler)
                        return
                    }
                }
            }
            do {
                if data != nil {
                    let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                    if dict != nil {
                        // debugPrint("Data received from Server is \(GeneralUtilities.printDictionary(dictionary: dict!))")
                    }
                }
            } catch {
                debugPrint("Error parsing data received: \(error.localizedDescription)")
            }
            weakSelf?.retryCount = 0
            completionHandler(data, response, error)
        }
        self.networkDataTask?.resume()
    }
}


