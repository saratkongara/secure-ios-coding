//
//  ViewController.swift
//  SecuritySuite
//
//  Created by Mohammad Haroon on 22/11/19.
//  Copyright © 2019 SecuritySuite. All rights reserved.
//

import UIKit
import IOSSecuritySuite

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnReverseEngineeringToolsDetector(_ sender: Any) {
        let amIReverseEngineered = IOSSecuritySuite.amIReverseEngineered()
        var message = "I am not Reverse Engineered"
        if amIReverseEngineered{
            message = "I am Reverse Engineered"
        }
        self.displayAlert(withMessage: message)
        
        
    }
    @IBAction func btnEmulatordetectormodule(_ sender: Any) {
        let runInEmulator = IOSSecuritySuite.amIRunInEmulator() ? true : false
        var message = "Not Running on Simulator"
        if runInEmulator{
            message = "Running on Simulator"
        }
        self.displayAlert(withMessage: message)
    }
    @IBAction func btnDenydebuggeratall(_ sender: Any) {
        IOSSecuritySuite.denyDebugger()
        self.displayAlert(withMessage: "Denied Debugger")
    }
    @IBAction func btnDebbugerdetectormodule(_ sender: Any) {
        let amIDebugged = IOSSecuritySuite.amIDebugged() ? true : false
        var message = "I am not Debugged"
        if amIDebugged{
            message = "I am not Debugged";
        }
        self.displayAlert(withMessage: message)
        
    }
    @IBAction func btnJailbreakdetectormodule(_ sender: Any) {
        var message = "This device is jailbroken"
        if IOSSecuritySuite.amIJailbroken() {
            message = "This device is jailbroken"
        } else {
            message = "This device is not jailbroken"
        }
        self.displayAlert(withMessage: message)
        
    }
    @IBAction func btnJailbrokenWithFailMessage(_ sender: Any) {
        var message = "This device is jailbroken"
        let jailbreakStatus = IOSSecuritySuite.amIJailbrokenWithFailMessage()
        if jailbreakStatus.jailbroken {
            message = "This device is jailbroken"
            message = "Because: \(jailbreakStatus.failMessage)"
        } else {
            message = "This device is not jailbroken"
        }
        self.displayAlert(withMessage: message)
    }
    
    
}

extension UIViewController{
    func displayAlert(withMessage alertMessage: String, withoutTitle: Bool = false) {
        var title = ""
        if !withoutTitle { title = "Enchipers" }
        let alert = UIAlertController(title: title, message: alertMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
